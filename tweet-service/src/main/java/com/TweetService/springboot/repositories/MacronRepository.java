package com.TweetService.springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.TweetService.springboot.model.Macron;

import java.util.List;

@Repository
public interface MacronRepository extends JpaRepository<Macron, Long> {

    List<Macron> findById(String id);

    List<Macron> findByText(String text);

    List<Macron> findByUsername(String username);

    List<Macron> findByDate(String date);
    
    List<Macron> findByTextContaining(String keyWord);
    
    List<Macron> findByTextContainingOrderByDateDesc(String keyWord);

}
