package com.TweetService.springboot.repositories;

import com.TweetService.springboot.model.Tweet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.TweetService.springboot.model.Macron;

import java.util.List;

@Repository
public interface TweetRepository extends JpaRepository<Tweet, Long> {

    List<Tweet> findById(String id);

    List<Tweet> findByText(String text);

    List<Tweet> findByUsername(String username);

    List<Tweet> findByDate(String date);
    
    List<Tweet> findByTextContaining(String keyWord);
    
    List<Tweet> findByTextContainingOrderByDateDesc(String keyWord);

}
