package com.TweetService.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.TweetService.springboot.configuration.JpaConfiguration;


@Import(JpaConfiguration.class)
@SpringBootApplication(scanBasePackages={"com.TweetService.springboot"})
public class TweetService {

	public static void main(String[] args) {
		
		SpringApplication.run(TweetService.class, args);
	}
}
