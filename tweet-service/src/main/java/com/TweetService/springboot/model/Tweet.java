package com.TweetService.springboot.model;

import javax.persistence.*;

@Entity
@Table(name="tweet")
public class Tweet {
		
		@Id
		@GeneratedValue(strategy= GenerationType.IDENTITY)
		private Long id;
		
		@Column(name="date")
		private String date;
		
		@Column(name="text")
		private String text;
		
		@Column(name="username")
		private String username;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		@Override
		public String toString() {
			return "Tweet [id=" + id + ", date=" + date + ", text=" + text + ", username=" + username + "]";
		}
		

		
}