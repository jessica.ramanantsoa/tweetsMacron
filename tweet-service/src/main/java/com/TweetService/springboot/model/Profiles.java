package com.TweetService.springboot.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name="profiles")
public class Profiles implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="username")
	private String username;
	
	@Column(name="sex")
	private String sex;
	
	@Column(name="fname")
	private String fname;
	
	@Column(name="lname")
	private String lname;
	
	@Column(name="mail")
	private String mail;
	
	@Column(name="bday")
	private Date bday;
	
	@Column(name="numaddress")
	private String numaddress;
	
	@Column(name="nameaddress")
	private String nameaddress;
	
	@Column(name="cp")
	private String cp;
	
	@Column(name="numaddressfav")
	private String numaddressfav;
	
	@Column(name="nameaddressfav")
	private String nameaddressfav;
	
	@Column(name="cpfav")
	private String cpfav;
	
	@Column(name="numaddressfav2")
	private String numaddressfav2;
	
	@Column(name="nameaddressfav2")
	private String nameaddressfav2;
	
	@Column(name="cpfav2")
	private String cpfav2;
	
	@Column(name="num")
	private String num;
	
	@Column(name="perimetre")
	private Integer perimetre;
	
	@Column(name="profile")
	private boolean profile;
	
	@Column(name="abo")
	private String abo;
	
	public Profiles() {}
	
	public Profiles(Long id, String username, String sex, String fname, String lname, String mail, Date bday,
			String numaddress, String nameaddress, String cp, String numaddressfav, String nameaddressfav, String cpfav,
			String numaddressfav2, String nameaddressfav2, String cpfav2, String num, Integer perimetre,
			boolean profile, String abo) {
		super();
		this.id = id;
		this.username = username;
		this.sex = sex;
		this.fname = fname;
		this.lname = lname;
		this.mail = mail;
		this.bday = bday;
		this.numaddress = numaddress;
		this.nameaddress = nameaddress;
		this.cp = cp;
		this.numaddressfav = numaddressfav;
		this.nameaddressfav = nameaddressfav;
		this.cpfav = cpfav;
		this.numaddressfav2 = numaddressfav2;
		this.nameaddressfav2 = nameaddressfav2;
		this.cpfav2 = cpfav2;
		this.num = num;
		this.perimetre = perimetre;
		this.profile = profile;
		this.abo = abo;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Date getBday() {
		return bday;
	}

	public void setBday(Date bday) {
		this.bday = bday;
	}

	public String getNumaddress() {
		return numaddress;
	}

	public void setNumaddress(String numaddress) {
		this.numaddress = numaddress;
	}

	public String getNameaddress() {
		return nameaddress;
	}

	public void setNameaddress(String nameaddress) {
		this.nameaddress = nameaddress;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getNumaddressfav() {
		return numaddressfav;
	}

	public void setNumaddressfav(String numaddressfav) {
		this.numaddressfav = numaddressfav;
	}

	public String getNameaddressfav() {
		return nameaddressfav;
	}

	public void setNameaddressfav(String nameaddressfav) {
		this.nameaddressfav = nameaddressfav;
	}

	public String getCpfav() {
		return cpfav;
	}

	public void setCpfav(String cpfav) {
		this.cpfav = cpfav;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public boolean getProfile() {
		return profile;
	}

	public void setProfile(boolean profile) {
		this.profile = profile;
	}

	public String getAbo() {
		return abo;
	}

	public void setAbo(String abo) {
		this.abo = abo;
	}

	public String getNumaddressfav2() {
		return numaddressfav2;
	}

	public void setNumaddressfav2(String numaddressfav2) {
		this.numaddressfav2 = numaddressfav2;
	}

	public String getNameaddressfav2() {
		return nameaddressfav2;
	}

	public void setNameaddressfav2(String nameaddressfav2) {
		this.nameaddressfav2 = nameaddressfav2;
	}

	public String getCpfav2() {
		return cpfav2;
	}

	public void setCpfav2(String cpfav2) {
		this.cpfav2 = cpfav2;
	}

	public Integer getPerimetre() {
		return perimetre;
	}

	public void setPerimetre(Integer perimetre) {
		this.perimetre = perimetre;
	}

	@Override
	public String toString() {
		return "Profiles [id=" + id + ", username=" + username + ", sex=" + sex + ", fname=" + fname + ", lname="
				+ lname + ", mail=" + mail + ", bday=" + bday + ", numaddress=" + numaddress + ", nameaddress="
				+ nameaddress + ", cp=" + cp + ", numaddressfav=" + numaddressfav + ", nameaddressfav=" + nameaddressfav
				+ ", cpfav=" + cpfav + ", numaddressfav2=" + numaddressfav2 + ", nameaddressfav2=" + nameaddressfav2
				+ ", cpfav2=" + cpfav2 + ", num=" + num + ", perimetre=" + perimetre + ", profile=" + profile + ", abo="
				+ abo + "]";
	}
	
}
