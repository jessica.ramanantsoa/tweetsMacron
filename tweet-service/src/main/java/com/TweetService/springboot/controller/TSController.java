package com.TweetService.springboot.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.TweetService.springboot.model.Macron;
import com.TweetService.springboot.service.MacronService;

@Controller
public class TSController {

	@Autowired
	MacronService mService; //Service which will do all data retrieval/manipulation work
	
	public static final Logger logger = LoggerFactory.getLogger(TSController.class);

	// -------------------Redirige vers index---------------------------------------
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String index(Model my_model) {
		return ("index");
	}

	//------------Verifie le mdp et redirige en consequence--------------------------
	@RequestMapping(value = "/accueilTweets")
	public ModelAndView checkMdp(Model model, WebRequest request) {

		ModelAndView mav;
		String id = request.getParameter("id");
		String mdp = request.getParameter("mdp");
		String exp = "LeCodeSecret";

		if(mdp.equals(exp)) 
			mav = new ModelAndView("recherche");
		else
			mav = new ModelAndView("index");

		model.addAttribute("id", id);

		return mav;
	}

	//------------recupere les tweets par mot cle--------------------------
	@RequestMapping(value = "/viewTweets")
	public ModelAndView recupTweets(Model model, WebRequest request) {

		List<Macron> mlist;
		ModelAndView mav = new ModelAndView("tweetsPage");
		
		String keyWord = request.getParameter("keyW");
		mlist = mService.findByTextContaining(keyWord);
		mav.addObject(mlist);

		return mav;
	}


}
