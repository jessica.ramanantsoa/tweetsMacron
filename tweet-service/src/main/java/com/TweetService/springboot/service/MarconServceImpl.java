package com.TweetService.springboot.service;

import java.util.List;


import com.TweetService.springboot.repositories.MacronRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.TweetService.springboot.model.Macron;
import com.TweetService.springboot.repositories.TweetRepository;

@Service("macronService")
@Transactional
public class MarconServceImpl implements MacronService {

	@Autowired
    private MacronRepository macronRepository;

	@Override
	public List<Macron> findById(String id) {
		// TODO Auto-generated method stub
		return macronRepository.findById(id);
	}

	@Override
	public List<Macron> findByText(String text) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Macron> findByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Macron> findByDate(String date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Macron> findByTextContaining(String keyWord) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Macron> findByTextContainingOrderByDateDesc(String keyWord) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
