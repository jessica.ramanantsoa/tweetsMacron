package com.TweetService.springboot.service;

import java.util.List;

import com.TweetService.springboot.model.Tweet;

public interface TweetService {

    List<Tweet> findById(String id);

    List<Tweet> findByText(String text);

    List<Tweet> findByUsername(String username);

    List<Tweet> findByDate(String date);
    
    List<Tweet> findByTextContaining(String keyWord);
    
    List<Tweet> findByTextContainingOrderByDateDesc(String keyWord);
	
}