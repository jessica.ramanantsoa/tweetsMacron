package com.TweetService.springboot.service;

import java.util.List;


import com.TweetService.springboot.repositories.TweetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.TweetService.springboot.model.Tweet;
import com.TweetService.springboot.repositories.MacronRepository;

@Service("tweetService")
@Transactional
public class TweetServceImpl implements TweetService {

	@Autowired
    private TweetRepository tweetRepository;

	@Override
	public List<Tweet> findById(String id) {
		// TODO Auto-generated method stub
		return tweetRepository.findById(id);
	}

	@Override
	public List<Tweet> findByText(String text) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Tweet> findByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Tweet> findByDate(String date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Tweet> findByTextContaining(String keyWord) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Tweet> findByTextContainingOrderByDateDesc(String keyWord) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
