package com.TweetService.springboot.service;

import java.util.List;

import com.TweetService.springboot.model.Macron;

public interface MacronService {

    List<Macron> findById(String id);

    List<Macron> findByText(String text);

    List<Macron> findByUsername(String username);

    List<Macron> findByDate(String date);
    
    List<Macron> findByTextContaining(String keyWord);
    
    List<Macron> findByTextContainingOrderByDateDesc(String keyWord);
	
}