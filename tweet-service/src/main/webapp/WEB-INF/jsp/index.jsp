<!DOCTYPE html>
<jsp:include page="header.jsp"></jsp:include>

<h1>Authentification</h1>

<h4>Pour acc�der au site d'analyseur de tweets personnalis�,
	veuillez entrer ci-apr�s le mot de passe qui vous a �t� fourni.</h4>
<br>
<form action="/TweetsMacron/viewTweets" method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td><b>Votre nom</b></td>
			<td><input type='text' name='id' class='form-control' required /></td>
		</tr>
		<tr>
			<td><b>Mot de passe</b></td>
			<td><input type='password' name='mdp' class='form-control' required /></td>
		</tr>
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">Acc�der au
					site</button>
			</td>
		</tr>
	</table>
</form>

<jsp:include page="footer.jsp"></jsp:include>