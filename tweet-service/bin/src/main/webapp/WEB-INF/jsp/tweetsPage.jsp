<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<style>
			.nav-pills-custom>li>a {
	color: lightgrey;
}

.nav-pills-custom>li>a:focus,
.nav-pills-custom>li>a:hover {
	background: none;
	color: grey;
}

.panel-custom>.panel-heading {
	background: none;
	border-bottom: none;
}

.navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:focus, .navbar-default .navbar-nav>.active>a:hover{
	background-color: #337AB7;
}
.form-control-nav{
	background-color:#F0FFFF;
  border: lightgrey solid 1px;
	border-radius:15px;
}
body{
	background-color: #F0FFFF;
}
.navbar-default .navbar-collapse, .navbar-default{
background-color: white;
}
.panel-footer{
	background-color: lightcyan;
}
.well{
	background-color: lightcyan;
}
a{
	color:#60A6C1;
}
div.panel-heading{
	padding-top:15px;
	padding-bottom: 0px;
}
.form-control-feedback{
	line-height:27px;
}
.panel-info{
	border:1px lightgrey solid;
}



		</style>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
		<!-- Bootstrap core CSS -->
		<link href="ressources/css/bootstrap.min.css" rel="stylesheet">
		<link href="/ressources/css/mdb.min.css" rel="stylesheet">
		<!--clock picker-->
		<link href="ressources/clockpicker-gh-pages/src/clockpicker.css" rel="stylesheet">
		<!-- Material Design Bootstrap -->
		<link href="ressources/css/mdb.min.css" rel="stylesheet">
		<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="ressources/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="ressources/clockpicker-gh-pages/src/clockpicker.js"></script>

<!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script>
	/**
 * French translation for bootstrap-datepicker
 * Nico Mollet <nico.mollet@gmail.com>
 */
;(function($){
	$.fn.datepicker.dates['fr'] = {
		days: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
		daysShort: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
		daysMin: ["d", "l", "ma", "me", "j", "v", "s"],
		months: ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
		monthsShort: ["janv.", "févr.", "mars", "avril", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc."],
		today: "Aujourd'hui",
		monthsTitle: "Mois",
		clear: "Effacer",
		weekStart: 1,
		format: "yyyy-mm-dd"
	};
}(jQuery));
    $(document).ready(function(){
      var date_input=$('input[name="date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
		language: 'fr',
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>
	</head>
	<body>
		<nav class="navbar navbar-dark special-color">
			<a class="navbar-brand" style="color:white;" ><i class="fa fa-twitter"></i> Tweets Macron</a>
		</nav>
		
				<br>
					<div class="panel-body">
						<div class="media">
							<a class="media-left">
								<img alt="" class="media-object img-rounded" src="ressources/twitter.png">
							</a>
								<div class="media-body">
									<h4 class="media-heading">Name @userName</h4>
									<p>Dolorem aspernatur rerum, iure? Culpa iste aperiam sequi, fuga, quasi rerum, eum, quo natus tenetur officia placeat.
									Dolorem aspernatur rerum, iure? Culpa iste aperiam sequi, fuga, quasi rerum, eum, quo natus tenetur officia placeat.
									Dolorem aspernatur rerum, iure? Culpa iste aperiam sequi, fuga, quasi rerum, eum, quo natus tenetur officia placeat.</p>
									<ul class="nav nav-pills nav-pills-custom">
										
									</ul>
								</div>
							</div>

							<div class="media">
								<a class="media-left">
									<img alt="" class="media-object img-rounded" src="ressources/twitter.png">
								</a>
								<div class="media-body">
									<h4 class="media-heading">Media heading</h4>
									<p>Dolorem aspernatur rerum, iure? Culpa iste aperiam sequi, fuga, quasi rerum, eum, quo natus tenetur officia placeat.</p>
									<ul class="nav nav-pills nav-pills-custom">
										
									</ul>
								</div>

							</div>	

					</div>
			
				</div>
			
			
	</div>	
		
	</body>
</html>
