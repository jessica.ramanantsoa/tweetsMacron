<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Tweets analyseur</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
<link href="/css/sticky-footer-navbar.css" rel="stylesheet" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">

			<ul class="nav navbar-nav">
				<li class="active"><a href="tweetsPage"
					title="Retour à la page d'authentification">Parce que c'est
						notre projet !</a></li>
			</ul>
		</div>
	</nav>
	<div class="container">

		<h1>Authentification</h1>

		<h4>Pour acceder au site d'analyseur de tweets personnalise,
			veuillez entrer ci-apres le mot de passe qui vous a ete fourni.</h4>
		<br>
		<form action='https://twitter-project.azurewebsites.net' onsubmit="verifForm(this)" method="post">
			<table class='table table-hover table-responsive table-bordered'>
				<tr>
					<td><b>Votre nom</b></td>
					<td><input type='text' name='nom' class='form-control' /></td>
				</tr>
				<tr>
					<td><b>Le mot de passe</b></td>
					<td><input type='password' name='mdp' class='form-control'
						required onblur="verif(this)"/></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<button type="submit" class="btn btn-primary">Valider</button>
					</td>
				</tr>
			</table>
		</form>
		<script>
			function verif(champ) {
				if (vhamp.value === "lecode") {
					return true;
				} else {
					return false;
				}
			}
			
			function verifForm(f)
			{
			   var mdpOk = verif(f.mdp);
			   
			   if(mdpOk)
			      return true;
			   else
			   {
			      alert("Veuillez saisir le bon mot de passe");
			      return false;
			   }
			}
		</script>
		<footer class="footer">
			<div class="container">
				<p class="text-muted"></p>
			</div>
		</footer>
</body>
</html>