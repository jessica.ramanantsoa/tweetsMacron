import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import com.microsoft.sqlserver.jdbc.SQLServerDriver;
public class Main {

    public static void main(String[] args) throws TwitterException, SQLException, ClassNotFoundException, ParseException {
        final SQLsrvConn sql=new SQLsrvConn();
       ConfigurationBuilder cb=new ConfigurationBuilder();
        cb.setOAuthConsumerKey("WD92HjkFeZF3UaNkON5OwmFSY").setOAuthConsumerSecret("GL2TypKAMxUait4Bo9QwNSGkSfYuqY9Q9kd0WV1ArU8TuEjG38").setOAuthAccessToken("968884229575757824-9d8a2541sEumca7y2TczYJas3pyqz71").setOAuthAccessTokenSecret("nEcSDhTQPesv6pMLHnosmq1nwWjZ1xSpr75KwVgpoxdQx");
        TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
		//Récupère les sujets tendances du jour et les Tweets qui les concernent
        GetTrends t = new GetTrends();
        final String[] trends= t.getTrends();
		
		//Ajout d'un Listener qui pour chaque statut obtenu après filtrage soit enregistré dans la base de données
        twitterStream.addListener(new StatusListener () {
            @Override
            public void onException(Exception e) {

            }

            public void onStatus(Status status) {

                if (status.isRetweet()==false){//le stream ne prend que les tweets qui ne sont pas des retweets. Sinon trop de doublons
                    try {
                        Tweet t=new Tweet(status.getText().replace("'",""),status.getUser().getScreenName(),status.getCreatedAt().toString()); //j'ai mis un replace car "," provoque des erreurs SQL
                        if(status.getText().contains("Macron") || status.getText().contains("macron")){sql.saveMacron(t);} //sauvegarde les tweets sur Macron
                        else{sql.saveTweet(t);}//sauvegarde les tweets sur les trends
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {

            }

            @Override
            public void onTrackLimitationNotice(int i) {

            }

            @Override
            public void onScrubGeo(long l, long l1) {

            }

            @Override
            public void onStallWarning(StallWarning stallWarning) {

            }});
			
			//avec FiltreQuery le TwitterStream ne retourne que les tweets en français et ceux qui concernent les sujets populaires du jour

            FilterQuery tweetFilterQuery = new FilterQuery();
            tweetFilterQuery.track(trends);
            tweetFilterQuery.language(new String[]{"fr"});

            twitterStream.filter(tweetFilterQuery);

        }}