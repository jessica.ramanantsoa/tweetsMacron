/* Cette classe permet la connexion avec notre base de données sur microsoft Azure
La connexion ne marche pas sur le wifi de l'ESIPE à cause des règles firewall
Il faut spécifier l'adresse IP du TweetScraper dans Azure pour autoriser la connexion
*/
import java.sql.*;

public class SQLsrvConn {
    Connection connection;

    public SQLsrvConn() throws SQLException, ClassNotFoundException {
        String hostName = "jess-data.database.windows.net";
        String dbName = "TweetMacronDB";
        String user = "jess";
        String password = "Azertyqwerty1";
        String url = String.format("jdbc:sqlserver://%s:1433;database=%s;user=%s;password=%s;encrypt=true;hostNameInCertificate=*.database.windows.net;loginTimeout=30;", hostName, dbName, user, password);
connection = DriverManager.getConnection(url);}

    public void closeConn() throws SQLException {connection.close();}


//Méthodes pour enregistrer des Tweets dans la base	
public void saveTweet(Tweet t) throws SQLException {

    String selectSql = "INSERT INTO tweet (date, text, username) VALUES ('"+t.getDate()+"','"+t.getText()+"','"+t.getUserName()+"')";
    Statement statement = connection.createStatement();
    statement.executeUpdate(selectSql);

    }
    public void saveMacron(Tweet t) throws SQLException {

    String selectSql = "INSERT INTO macron (date, text, username) VALUES ('"+t.getDate()+"','"+t.getText()+"','"+t.getUserName()+"')";
    Statement statement = connection.createStatement();
    statement.executeUpdate(selectSql);

    }
}