import java.util.Date;

public class Tweet {
    private Integer id;
    private String text;
    private String userName;
    private String date;
    public Tweet(String text, String userName, String date){
        this.id=null;
        this.text=text;
        this.userName=userName;
        this.date=date;
    }

    public String getDate() {
        return date;
    }

    public Integer getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public String toString() {
        return " username = "+this.userName+
                "content = "+this.text+
                " created At = "+this.date+" ";
    }
}
